#!/usr/bin/env python3

import curses


class State:
    def __init__(self):
        self.thing = 0


def main(stdscr):
    stdscr.clear()

    curses.mousemask(1)

    while True:
        c = stdscr.getch()
        if c == ord("q"):
            break
        # elif c == ord("w"):
        #     y, x = stdscr.getyx()
        #     stdscr.move(y + 1, x + 1)
        #     stdscr.addch("!")
        elif c == curses.KEY_MOUSE:
            _, x, y, _, _ = curses.getmouse()
            stdscr.move(y, x)
            stdscr.addstr("0")


if __name__ == "__main__":
    curses.wrapper(main)
